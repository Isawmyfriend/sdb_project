const PM25Model = require("../model/PM25Model");

class PM25Controller {
  async getMutiPoint() {
    const { result, message, error } = await new PM25Model().getMutiPoint();
    return { result: result, message: message, error: error };
  }

  async getMutiPointbyYearandCountry(year, country) {
    const { result, message, error } =
      await new PM25Model().getMutiPointbyYearandCountry(year, country);
    return { result: result, message: message, error: error };
  }

  async getMutiPointbyYearandWbinc16_text(year, Wbinc16_text) {
    const { result, message, error } =
      await new PM25Model().getMutiPointbyYearandWbinc16_text(
        year,
        Wbinc16_text
      );
    return { result: result, message: message, error: error };
  }

  async getMutiPointbyYear(year) {
    const { result, message, error } = await new PM25Model().getMutiPointbyYear(
      year
    );
    return { result: result, message: message, error: error };
  }

  async getMutiPointbyCountry(country) {
    const { result, message, error } =
      await new PM25Model().getMutiPointbyCountry(country);
    return { result: result, message: message, error: error };
  }

  async getMutiPointbyWbinc16_text(Wbinc16_text) {
    const { result, message, error } =
      await new PM25Model().getMutiPointbyWbinc16_text(Wbinc16_text);
    return { result: result, message: message, error: error };
  }

  async getCountry() {
    const { result, message, error } = await new PM25Model().getCountry();
    return { result: result, message: message, error: error };
  }

  async getYear() {
    const { result, message, error } = await new PM25Model().getYear();
    return { result: result, message: message, error: error };
  }

  async getWbinc16_text() {
    const { result, message, error } = await new PM25Model().getWbinc16_text();
    return { result: result, message: message, error: error };
  }

  //Exercise

  async queryNoB() {
    const { result, message, error } = await new PM25Model().queryNoB();
    return { result: result, message: message, error: error };
  }

  async queryNoC() {
    const { result, message, error } = await new PM25Model().queryNoC();
    return { result: result, message: message, error: error };
  }

  async queryNoD() {
    const { result, polygon, message, error } =
      await new PM25Model().queryNoD();
    return { result: result, polygon: polygon, message: message, error: error };
  }

  async queryNoE() {
    const { result, message, error } = await new PM25Model().queryNoE();
    return { result: result, message: message, error: error };
  }
}

module.exports = PM25Controller;
