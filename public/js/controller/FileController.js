const FileModel = require("../model/FileModel");

class FileController {
  async insertCSVtoDB(filename) {
    const { message, error } = await new FileModel().insertCSVtoDB(filename);
    return { message: message, error: error };
  }

  async queryFileNoa() {
    const { file, message, error } = await new FileModel().queryFileNoa();
    return { file: file, message: message, error: error };
  }

  async queryFileNob() {
    const { file, message, error } = await new FileModel().queryFileNob();
    return { file: file, message: message, error: error };
  }

  async queryFileNoc(country) {
    const { file, message, error } = await new FileModel().queryFileNoc(
      country
    );
    return { file: file, message: message, error: error };
  }

  async queryFileNod(year, colorpm25) {
    const { file, message, error } = await new FileModel().queryFileNod(
      year, colorpm25
    );
    return { file: file, message: message, error: error };
  }
}

module.exports = FileController;
