import Map from "https://js.arcgis.com/4.23/@arcgis/core/Map.js";
import MapView from "https://js.arcgis.com/4.23/@arcgis/core/views/MapView.js";
import Legend from "https://js.arcgis.com/4.23/@arcgis/core/widgets/Legend.js";
import esriConfig from "https://js.arcgis.com/4.23/@arcgis/core/config.js";
import GeoJSONLayer from "https://js.arcgis.com/4.23/@arcgis/core/layers/GeoJSONLayer.js";

esriConfig.apiKey =
  "AAPK5abcfc9f607049c18510abd2ee5845a2Rmxnq0-ifuI_9rHW32rjMIWPGo0gygGfMKl4he_3xCSVIoIw6kNcEAET3lwLLyug";

const pm25Data = await getMutiPoint();

const blob = await new Blob([JSON.stringify(pm25Data)], {
  type: "application/json",
});
const url = await URL.createObjectURL(blob);

const template = {
  title: "PM 2.5 Info",
  content:
    "Country {Country}, {City} in {year} | PM25: {Pm25} | Wbinc16_text: {Wbinc16_text}",
  fieldInfos: [
    {
      fieldName: "time",
      format: {
        dateFormat: "short-date-short-time",
      },
    },
  ],
};

const renderer = {
  type: "simple",
  field: "Pm25",
  symbol: {
    type: "simple-marker",
    color: [250, 250, 250],
    outline: {
      color: [82, 84, 84],
    },
  },

  visualVariables: [
    {
      type: "size",
      field: "Pm25",
      stops: [
        {
          value: 5,
          size: "7px",
        },
        {
          value: 200,
          size: "25px",
        },
      ],
    },
    {
      type: "color",
      field: "Pm25",
      stops: [
        { value: 1, color: "#6cdf5c" },
        { value: 10, color: "#ffffbf" },
        { value: 25, color: "#fdae61" },
        { value: 35, color: "#d7191c" },
        { value: 50, color: "#921012" },
      ],
    },
  ],
};

const geojsonLayer = new GeoJSONLayer({
  url: url,
  copyright: "GIS ",
  popupTemplate: template,
  renderer: renderer,
  orderBy: {
    field: "Pm25" * 0.1,
  },
});

const map = new Map({
  basemap: "arcgis-colored-pencil", // Basemap layer
  layers: [geojsonLayer],
});

const view = new MapView({
  map: map,
  center: [100.505513, 13.743732],
  zoom: 5, 
  container: "viewDiv",
  constraints: {
    snapToZoom: true,
    minZoom: 2,
  },
  extent: {
    spatialReference: 4326,
  },
});

view.ui.add(
  new Legend({
    view: view,
  })
);

async function getMutiPoint() {
  try {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const wbinc16 = urlParams.get("wbinc16");
    const year = urlParams.get("year");

    if (wbinc16 === null || year === null) {
      const data = await axios.get("/pm25multipoint");
      return data.data.result;
    } else {
      const data = await axios.get(`/queryNoF?year=${year}&wbinc16=${wbinc16}`);
      return data.data.result;
    }
  } catch (error) {
    return [];
  }
}
