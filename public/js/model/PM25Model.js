const config = require("../../../database");
const sql = require("mssql");


var connectWithRetry = async () => {
  return await sql.connect(config, (err) => {
    if (err) {
      console.log('Connection to DB failed, retry in 5s')
      sql.close();
      setTimeout(connectWithRetry, 5000);
    } else {
      console.log('Connection to DB is now ready...');
    }
 });
}

class PM25Model {

  
  async getMutiPoint() {
    try {

      await connectWithRetry()
      //Query Data from Database
      const result = await sql.query(
        "select * from AirPollutionPM25"
      );

      //Return Data to Controller
      return {
        result: await this.createTemplate(result.recordset),
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Data",
        error: true,
      };
    }
  }

  //Exercise 5.A
  async getMutiPointbyYearandCountry(year, country) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select  top 500 * from AirPollutionPM25 where Year = '${year}' and Country = '${country}'`
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async getMutiPointbyYear(year) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select  top 500 * from AirPollutionPM25 where Year = '${year}' `
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async getMutiPointbyCountry(country) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select  top 500 * from AirPollutionPM25 where Country = '${country}'`
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async getMutiPointbyWbinc16_text(Wbinc16_text) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select  top 500 * from AirPollutionPM25 where Wbinc16_text = '${Wbinc16_text}'`
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async getMutiPointbyYearandWbinc16_text(year, Wbinc16_text) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select  top 500 * from AirPollutionPM25 where Year = '${year}' and Wbinc16_text = '${Wbinc16_text}'`
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Data",
        error: true,
      };
    }
  }

  //Get country for Select in HTML
  async getCountry() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select Country from AirPollutionPM25 group by Country order by Country`
      );

      return {
        result: await result.recordset,
        message: "Get Country Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Country",
        error: true,
      };
    }
  }

   //Get years for Select in HTML
  async getYear() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select Year from AirPollutionPM25 group by Year order by Year`
      );

      return {
        result: await result.recordset,
        message: "Get Year Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Year",
        error: true,
      };
    }
  }

   //Get Wbinc16_text for Select in HTML
  async getWbinc16_text() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select Wbinc16_text from AirPollutionPM25 group by Wbinc16_text order by Wbinc16_text`
      );

      return {
        result: await result.recordset,
        message: "Get Wbinc16 Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get Wbinc16",
        error: true,
      };
    }
  }

  //Exercise 5.B
  async queryNoB() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `DECLARE @TH geometry

        SELECT @TH=geom
         FROM AirPollutionPM25
         WHERE country='Thailand' AND City='Bangkok'
        
        
         SELECT TOP(50) * FROM AirPollutionPM25  WHERE geom IS NOT NULL
        ORDER BY geom.STDistance(@TH);  `
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get No B Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get No B",
        error: true,
      };
    }
  }

  //Exercise 5.C
  async queryNoC() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `DECLARE @TH geometry

        SELECT @TH=geom
         FROM AirPollutionPM25
         WHERE country='Thailand' AND City='Bangkok'
        
        
         SELECT TOP(100) * FROM AirPollutionPM25  WHERE geom IS NOT NULL AND Country != 'Thailand'
        ORDER BY geom.STDistance(@TH);  `
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get No C Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get No C",
        error: true,
      };
    }
  }

  //Exercise 5.D
  async queryNoD() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `DECLARE @TH geometry
        SELECT @TH=geometry::EnvelopeAggregate(geom)
        FROM AirPollutionPM25
        WHERE Country ='Thailand'
       
        SELECT @TH.STEnvelope() AS Polygon `
      );

      const resultThailand = await sql.query(
        `select * from AirPollutionPM25 where Country = 'Thailand'`
      );

      let FeatureCollection = await this.createTemplate(
        resultThailand.recordset
      );

      const polygonPoint = result.recordset[0].Polygon.points;
      let polygonPointArray = [];

      polygonPoint.map((p) => {
        polygonPointArray.push([p.x, p.y]);
      });

      //Make the polygon data to be in GeoJSON format.
      let features = {
        type: "Feature",
        properties: {
          Info: "MBR covering all city points in Thailand in 2009.",
        },
        geometry: {
          type: "polygon",
          rings: [polygonPointArray],
        },
      };

      return {
        result: FeatureCollection,
        polygon: features,
        message: "Get No D Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        polygon: [],
        message: "Error to get No D",
        error: true,
      };
    }
  }

  //Exercise 5.E
  async queryNoE() {
    try {
      await connectWithRetry
      const result = await sql.query(
        `DECLARE @C VARCHAR(2550)

        select Top(1)  @C=Country from AirPollutionPM25 where Year = 2011 group by Country order by COUNT(City) DESC
        
        select * from AirPollutionPM25 where Country = @C  `
      );

      return {
        result: await this.createTemplate(result.recordset),
        message: "Get No E Complete",
        error: false,
      };
    } catch (error) {
      return {
        result: [],
        message: "Error to get No E",
        error: true,
      };
    }
  }

  //Make the raw data to be in GeoJSON format.
  async createTemplate(result) {
    try { 

      let FeatureCollection = { type: "FeatureCollection", features: [] };
      let features = {
          type: "Feature",
          geometry: { type: "Point", coordinates: [] },
          properties: {
              Country: "",
              City: "",
              Year: "",
              Pm25: "",
              Population: "",
              Wbinc16_text: "",
              Region: "",
              Conc_Pm25: "",
              Color_Pm25: "",
          },
      };

      result.forEach((p) => {

        features.properties.Country = p.Country;
        features.properties.City = p.City;
        features.properties.Year = p.Year;
        features.properties.Pm25 = p.Pm25;
        features.properties.Population = p.Population;
        features.properties.Wbinc16_text = p.Wbinc16_text;
        features.properties.Region = p.Region;
        features.properties.Conc_Pm25 = p.Conc_Pm25;
        features.properties.Color_Pm25 = p.Color_Pm25;

        let y = p.Latitude;
        let x = p.Longitude;
        let xyArrays = [x, y];

        features.geometry.coordinates = xyArrays;
        xyArrays = [];

        FeatureCollection.features.push(features);
        features = {
            type: "Feature",
            geometry: { type: "Point", coordinates: [] },
            properties: {
                Country: "",
                City: "",
                Year: "",
                Pm25: "",
                Population: "",
                Wbinc16_text: "",
                Region: "",
                Conc_Pm25: "",
                Color_Pm25: "",
            },
        };
       
      })

      
      return FeatureCollection;
    } catch (error) {
        return [];
    }
  }
}

module.exports = PM25Model;
