const config = require("../../../database");
const sql = require("mssql");
const fs = require("fs");
const fastcsv = require("fast-csv");
const path = require("path");
const uuid = require("uuid").v4;


var connectWithRetry = async () => {
  return await sql.connect(config, (err) => {
    if (err) {
      console.log('Connection to DB failed, retry in 5s')
      sql.close();
      setTimeout(connectWithRetry, 5000);
    } else {
      console.log('Connection to DB is now ready...');
    }
 });
}

class FileModel {


  //Get CSV file and Insert into DB
  async insertCSVtoDB(filename) {
    try {

      
      let stream = fs.createReadStream(
        path.join("public/assets/upload", filename)
      );
      let csvData = [];
      //Read CSV file by Fast-CSV
      let csvStream = fastcsv
        .parse()
        .on("data", function (data) {
          data[3] = parseFloat(data[3]);
          data[4] = parseFloat(data[4]);
          data[5] = parseFloat(data[5]);
          data[6] = parseInt(data[6]);
          csvData.push(data);
        })
        .on("end", async function () {
          
          csvData.shift();

          for (var i = 0; i < csvData.length; i++) {
            await sql.connect(config).then((pool) => {
              

              //Get Data from reading CSV and Insert to DB
              return pool
                .request()
                .input("Country", sql.VarChar, csvData[i][0])
                .input("City", sql.VarChar, csvData[i][1])
                .input("Year", sql.VarChar, csvData[i][2])
                .input("Pm25", sql.Float, csvData[i][3])
                .input("Latitude", sql.Float, csvData[i][4])
                .input("Longitude", sql.Float, csvData[i][5])
                .input("Population", sql.Int, csvData[i][6])
                .input("Wbinc16_text", sql.VarChar, csvData[i][7])
                .input("Region", sql.VarChar, csvData[i][8])
                .input("Conc_Pm25", sql.VarChar, csvData[i][9])
                .input("Color_Pm25", sql.VarChar, csvData[i][10])
                .query(
                  `insert into AirPollution2PM25  ( Country, City, Year, Pm25, Latitude, Longitude, Population, Wbinc16_text, Region, Conc_Pm25, Color_Pm25 )
                values (@Country,@City,@Year,@Pm25,@Latitude,@Longitude,@Population,@Wbinc16_text,@Region,@Conc_Pm25,@Color_Pm25);
                
                UPDATE AirPollution2PM25 
                SET geom = geometry::STGeomFromText( 'POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + CAST([Latitude] AS VARCHAR(20)) + ')',4326) where Latitude IS NOT NULL and Longitude IS NOT NULL;
                `
                );
            });

            console.log(`${i}/${csvData.length}`);
          }
        });

      stream.pipe(csvStream);

      return {
        message: "Upload File Complete.",
        error: false,
      };
    } catch (error) {
      return {
        message: "Error to Upload File.",
        error: true,
      };
    }
  }

  async queryFileNoa() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select Country,City from AirPollutionPM25 where Year = 2015 and Pm25 > 50 `
      );

      //Write CSV
      const filename = uuid();
      const ws = fs.createWriteStream(
        `public/assets/download/a/${filename}.csv`
      );

      fastcsv.write(result.recordset, { headers: true }).pipe(ws);

      const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
      await delay(1000);

      //Return File name for Download
      return {
        file: filename + ".csv",
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        file: "",
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async queryFileNob() {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select Country,AVG(Pm25) as Pm25AVG  from AirPollutionPM25 group by Country order by AVG(Pm25) DESC`
      );

      //Write CSV
      const filename = uuid();
      const ws = fs.createWriteStream(
        `public/assets/download/b/${filename}.csv`
      );

      fastcsv.write(result.recordset, { headers: true }).pipe(ws);

      const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
      await delay(1000);

      //Return File name for Download
      return {
        file: filename + ".csv",
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        file: "",
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async queryFileNoc(country) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select City,Year, Pm25 from AirPollutionPM25 where Country='${country}'`
      );

      //Write CSV
      const filename = uuid();
      const ws = fs.createWriteStream(
        `public/assets/download/c/${filename}.csv`
      );

      fastcsv.write(result.recordset, { headers: true }).pipe(ws);

      const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
      await delay(1000);

      //Return File name for Download
      return {
        file: filename + ".csv",
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        file: "",
        message: "Error to get Data",
        error: true,
      };
    }
  }

  async queryFileNod(year, colorpm25) {
    try {
      await connectWithRetry()
      const result = await sql.query(
        `select SUM(Population) as Affected_Population from AirPollutionPM25 where Year = ${year} and Color_Pm25 = '${colorpm25}'`
      );

      //Write CSV
      const filename = uuid();
      const ws = fs.createWriteStream(
        `public/assets/download/d/${filename}.csv`
      );

      fastcsv.write(result.recordset, { headers: true }).pipe(ws);

      const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
      await delay(1000);

       //Return File name for Download
      return {
        file: filename + ".csv",
        message: "Get Data Complete",
        error: false,
      };
    } catch (error) {
      return {
        file: "",
        message: "Error to get Data",
        error: true,
      };
    }
  }
}

module.exports = FileModel;
