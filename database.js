require("dotenv").config();

const config = {
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  server: process.env.DB_HOST,
  options: {
    trustedConnection: true,
    trustServerCertificate: true,
  },
  port: 1433,
};

module.exports = config;
