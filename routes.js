const PM25Controller = require("./public/js/controller/PM25Controller");
const FileController = require("./public/js/controller/FileController");
const multer = require("multer");
const { download } = require("express/lib/response");
const uuid = require("uuid").v4;
//Create Multer for Save File
let filename = "";
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/assets/upload");
  },
  filename: (req, file, cb) => {
    const { originalname } = file;
    filename = `${uuid()}-${originalname}`;
    cb(null, filename);
  },
});
const upload = multer({ storage });

module.exports = (app) => {
  //Render Index Page
  app.get("/", async (req, res) => {
    res.render("index");
  });

  //Render Map 1 Page
  app.get("/mapmain1", async (req, res) => {
    const country = await new PM25Controller().getCountry();
    const year = await new PM25Controller().getYear();

    if (country.error || year.error) {
      res.status(400).json({ message: country.message });
    }

    res.render("mapmain1", {
      country: country.result,
      year: year.result,
    });
  });

  app.get("/pm25multipoint", async (req, res) => {
    const { result, message, error } =
      await new PM25Controller().getMutiPoint();
    if (error) {
      res.status(400).json({ message: message, result: result });
    } else {
      res.status(200).json({ message: message, result: result });
    }
  });

  app.get("/pm25YearCountry", async (req, res) => {
    if (req.query.country == "N/A" && req.query.year == "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPoint();

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else if (req.query.country != "N/A" && req.query.year != "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPointbyYearandCountry(
          req.query.year,
          req.query.country
        );

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else if (req.query.year == "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPointbyCountry(req.query.country);

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else if (req.query.country == "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPointbyYear(req.query.year);

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else {
      const { result, message, error } =
        await new PM25Controller().getMutiPoint();

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    }
  });

  //Exercise 4
  app.get("/filemain", async (req, res) => {
    res.render("filemain");
  });

  app.post("/uploadCSV", upload.single("file"), async (req, res) => {
    const { message, error } = await new FileController().insertCSVtoDB(
      filename
    );

    if (error) {
      res.status(400).json({ message: message });
    } else {
      res.status(200).json({ message: message });
    }
  });

  app.get("/queryFileNoa", async (req, res) => {
    const { file, message, error } = await new FileController().queryFileNoa();

    if (error) {
      res.status(400).json({ file: file, message: message });
    } else {
      res.download("public/assets/download/a/" + file);
    }
  });

  app.get("/queryFileNob", async (req, res) => {
    const { file, message, error } = await new FileController().queryFileNob();

    if (error) {
      res.status(400).json({ file: file, message: message });
    } else {
      res.download("public/assets/download/b/" + file);
    }
  });

  app.get("/queryFileNoc", async (req, res) => {
    const { file, message, error } = await new FileController().queryFileNoc(
      req.query.country
    );

    if (error) {
      res.status(400).json({ file: file, message: message });
    } else {
      res.download("public/assets/download/c/" + file);
    }
  });

  app.get("/queryFileNod", async (req, res) => {
    const { file, message, error } = await new FileController().queryFileNod(
      req.query.year,
      req.query.colorpm25
    );

    if (error) {
      res.status(400).json({ result: result, message: message });
    } else {
      res.download("public/assets/download/d/" + file);
    }
  });

  //Exercise 5

      //Render Map 2 Page
  app.get("/mapmain2", async (req, res) => {
    res.render("mapmain2");
  });

      //Render Map 3 Page
  app.get("/mapmain3", async (req, res) => {
    const wbinc16 = await new PM25Controller().getWbinc16_text();
    const year = await new PM25Controller().getYear();

    if (wbinc16.error || year.error) {
      res.status(400).json({ message: wbinc16.message });
    }
    res.render("mapmain3", {
      wbinc16: wbinc16.result,
      year: year.result,
    });
  });

  app.get("/queryNoB", async (req, res) => {
    const { result, message, error } = await new PM25Controller().queryNoB();

    if (error) {
      res.status(400).json({ message: message, result: result });
    } else {
      res.status(200).json({ message: message, result: result });
    }
  });

  app.get("/queryNoC", async (req, res) => {
    const { result, message, error } = await new PM25Controller().queryNoC();

    if (error) {
      res.status(400).json({ message: message, result: result });
    } else {
      res.status(200).json({ message: message, result: result });
    }
  });

  app.get("/queryNoD", async (req, res) => {
    const { result, polygon, message, error } =
      await new PM25Controller().queryNoD();

    if (error) {
      res
        .status(400)
        .json({ message: message, result: result, polygon: polygon });
    } else {
      res
        .status(200)
        .json({ message: message, result: result, polygon: polygon });
    }
  });

  app.get("/queryNoD", async (req, res) => {
    const { result, polygon, message, error } =
      await new PM25Controller().queryNoD();

    if (error) {
      res
        .status(400)
        .json({ message: message, result: result, polygon: polygon });
    } else {
      res
        .status(200)
        .json({ message: message, result: result, polygon: polygon });
    }
  });

  app.get("/queryNoE", async (req, res) => {
    const { result, message, error } = await new PM25Controller().queryNoE();

    if (error) {
      res.status(400).json({ message: message, result: result });
    } else {
      res.status(200).json({ message: message, result: result });
    }
  });

  app.get("/queryNoF", async (req, res) => {
    if (req.query.year == "N/A" && req.query.wbinc16 == "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPoint();

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else if (req.query.year != "N/A" && req.query.wbinc16 != "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPointbyYearandWbinc16_text(
          req.query.year,
          req.query.wbinc16
        );

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else if (req.query.year == "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPointbyWbinc16_text(
          req.query.wbinc16
        );

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else if (req.query.wbinc16 == "N/A") {
      const { result, message, error } =
        await new PM25Controller().getMutiPointbyYear(req.query.year);

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    } else {
      const { result, message, error } =
        await new PM25Controller().getMutiPoint();

      if (error) {
        res.status(400).json({ message: message, result: result });
      } else {
        res.status(200).json({ message: message, result: result });
      }
    }
  });
};
