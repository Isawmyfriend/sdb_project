require("dotenv").config();
const cors = require("cors");
const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const exphbs = require("express-handlebars");

const app = express();
const PORT = process.env.PORT;

app.use("/", express.static(__dirname));
app.use("/public", express.static(path.resolve(__dirname, "public")));

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.engine("handlebars", exphbs.engine({ extname: ".handlebars" }));
app.set("views", path.join(__dirname, "public", "views"));
app.set("view engine", "handlebars");
require("./routes")(app);

app.listen(PORT, console.log(`Server is running on PORT ${PORT}`));
